import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import './App.css';
import About from './pages/About';
import Contact from './pages/Contact';
import Document from './pages/Document'
import Navbar from './pages/Navbar';
import Pdf from './pages/Pdf';
import Skills from './pages/Skills';


function App() {
  return (
    <>
      <Router>
        <Navbar/>
      <Routes>
        <Route path={`${process.env.PUBLIC_URL}/`} element={<Document />}/>
        <Route path={`${process.env.PUBLIC_URL}/pdf`} element={<Pdf />}/>
        <Route path={`${process.env.PUBLIC_URL}/skills`} element={<Skills />}/>
        <Route path={`${process.env.PUBLIC_URL}/contact`} element={<Contact />}/>
        <Route path={`${process.env.PUBLIC_URL}/about`} element={<About />}/>
      </Routes>
      </Router>
    </>
  );
}

export default App;
