import React from "react";
import Footer from "./footer";
import perfil from "../img/PERFIL.jpg";

const About = () => {
  return (
    <div className="SkillsContainer">
      <div className="AboutCont">
        <div class="imgContainer">
          <img src={perfil} alt="imgPerfil" className="imgPerfil" />
          <span className="">
            <span className="Aabout">A</span>
          </span>
          <span className="aboutMeSpan">
            <span className="Babout">bout Me</span>
            <span className="spanCircle2 circleAbout"> ●</span>
          </span>
          <div className="bioSpan">
               ""Front-End Developer<br/> passionate about making websites <br/> 
               intuitive and visually appealing."
        
          </div>
        </div>
        <p>
          Junior Front-end Developer with strong skills and a passion for
            learning. Ability to work effectively in a team and a willingness to
            acquire new knowledge. Committed to finding effective solutions to
            challenges and contributing to the company's growth and the success
            of the team. Open to advice and constructive criticism, with a
            positive and communicative approach. Self- taught and eager to share  
            acquired knowledge.<br/><br/>
          My education encompasses a diverse array of courses undertaken at
          FreeCodeCamp, where I've delved into a comprehensive understanding of
          key programming and web development areas to set me up for a good start as front-end developer.
          To evolve my technical skills I invested several months of study following online
          tutorials in multiple sites, such as Youtube courses, tutorials provided by Reactjs.com, 
          Udemy courses and others. I've taken 1:1 mentoring and coaching from experts in the field,
          <a className="anclaAbout" target="_blank" rel="noopener noreferrer" href='https://www.linkedin.com/in/cyntsanchez/'>&nbsp;Cynthia Sanchez</a>: Senior Front-end Developer and Sr. Technical PM at SUSE Linux GmbH, and 
          <a className="anclaAbout" target="_blank" rel="noopener noreferrer" href='https://www.linkedin.com/in/santiago-petrone/'>&nbsp;Santiago Petrone</a>: Senior Front-end Deverloper at Xing GmbH. Their mentoring has been instrumental
          to my growth and understanding of the intricate areas of software engineering and allowed 
          me to have a better understanding of the theorerical parts of everything I learnt previuosly.<br/><br/>
          I am excited about the opportunity to work in a team to tackle new challenges and continue honing my skills, while contributing to the growth of the company. I am eager to learn, collaborate, and grow alongside the talented professionals at the company, and I am dedicated to putting in the effort required to excel in the world of front-end development.
        </p>
      </div>
      <Footer />
    </div>
  );
};

export default About;
