import React, { useEffect } from 'react';
import './Pdf.css';
import FatimaCV from '../Docs/FatimaCV.pdf';

const Pdf = () => {
  useEffect(() => {
    if (typeof window.orientation !== 'undefined') {
      document.getElementById('downloadPdf').click();
      window.close();
    }
  }, []);

  return (
    <div className='pdfCont'>
      <object data={FatimaCV} type='application/pdf' width='100%' height='100%'>
        <br />
        <a href={FatimaCV} id='downloadPdf' download='FatimaCV.pdf'>
         <button className='btnDownload'>Click here to download</button> 
        </a>
      </object>
    </div>
  );
};

export default Pdf;
