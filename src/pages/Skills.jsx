import React from 'react'
import boostrap from '../img/skills/boostrapIc.png'
import tailwind from '../img/tailwind.svg'
import material from '../img/material.svg'
import typescript from '../img/typescript.svg'
import vsc from '../img/skills/vscode.png'
import adobe from '../img/skills/adobe.png'
import { FaHtml5, FaCss3Alt, FaJs,  FaReact, FaSass  } from 'react-icons/fa';
import Footer from './footer'


const Skills = () => {
  const FaVite = ({ size }) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={size}
      height={size}
      style={{ color: 'rgb(214, 253, 83)' }}
      role="img"
    >
      <title>Vite</title>
      <path
        d="M8.286 10.578.512-8.657a.306.306 0 0 1 .247-.282L17.377.006a.306.306 0 0 1 .353.385l-1.558 5.403a.306.306 0 0 0 .352.385l2.388-.46a.306.306 0 0 1 .332.438l-6.79 13.55-.123.19a.294.294 0 0 1-.252.14c-.177 0-.35-.152-.305-.369l1.095-5.301a.306.306 0 0 0-.388-.355l-1.433.435a.306.306 0 0 1-.389-.354l.69-3.375a.306.306 0 0 0-.37-.36l-2.32.536a.306.306 0 0 1-.374-.316zm14.976-7.926L17.284 3.74l-.544 1.887 2.077-.4a.8.8 0 0 1 .84.369.8.8 0 0 1 .034.783L12.9 19.93l-.013.025-.015.023-.122.19a.801.801 0 0 1-.672.37.826.826 0 0 1-.634-.302.8.8 0 0 1-.16-.67l1.029-4.981-1.12.34a.81.81 0 0 1-.86-.262.802.802 0 0 1-.165-.67l.63-3.08-2.027.468a.808.808 0 0 1-.768-.233.81.81 0 0 1-.217-.6l.389-6.57-7.44-1.33a.612.612 0 0 0-.64.906L11.58 23.691a.612.612 0 0 0 1.066-.004l11.26-20.135a.612.612 0 0 0-.644-.9z"
        fill="rgb(214, 253, 83)"
      ></path>
    </svg>
  );
  
  return (
    <div>    
      <div className='SkillsContainer'>
        <h2 >
        Programming<br/> Languages <span className='spanCircle2'>●</span>
        </h2>
        <div className='skillsTech'>
         <div className=''>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaHtml5 size={24} style={{marginRight:'10px'}}/>  HTML5</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaJs size={24} style={{marginRight:'10px'}}/>  Javascript</p>
         <img src={typescript} alt='boostrapImg' className='boostrapIcon' />
         <span>  &nbsp; Typescript</span>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaSass size={24} style={{marginRight:'10px'}}/>  SASS</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaCss3Alt size={24} style={{marginRight:'10px'}}/>  CSS</p>
         </div>
        </div>
        <h2 >
        Frameworks <br/>& Libraries <span className='spanCircle2'>●</span>
        </h2>
        <div className='techCont skillsTech'>
         <div className='techIcons'>
          <div className='boostrapIcon2'>
          <img src={tailwind} alt='boostrapImg' style={{marginTop:'10px' ,marginRight:'14px', width:'26px', }}/>
          <span style={{marginTop:'14px' }}>  Tailwind</span>
          </div>
          <img src={material} alt='boostrapImg' style={{marginTop:'10px' ,marginRight:'14px', width:'24px', }}/>
          <span>  Material UI</span> 
          <div className='boostrapIcon2'>
          <img src={boostrap} alt='boostrapImg' style={{marginTop:'10px' ,marginRight:'14px', width:'26px', }}/>
          <span style={{marginTop:'14px' }}>  Boostrap</span> 
          </div>
          <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/> &nbsp; React</p>
          <p style={{ display: 'flex', alignItems: 'center',}}><FaVite size={24} /> &nbsp; &nbsp;  Vite</p>
         </div>
        </div>
        <h2 >
        Software <br/>& Tools <span className='spanCircle2'>●</span>
        </h2>
        <div className='techCont skillsTech'>
         <div className='techIcons'>
          <div className='boostrapIcon2'>
            <img src={vsc} alt='boostrapImg' style={{marginTop:'10px' ,marginRight:'14px', width:'30px', }}/>
            <span style={{marginTop:'14px' }}>  VS Code</span>
          </div>
          <div className='boostrapIcon2'>
            <img src={adobe} alt='boostrapImg' style={{marginTop:'10px' ,marginRight:'14px', width:'30px', }}/>
            <span style={{marginTop:'14px' }}>  Adobe</span>
          </div>
          </div>
          </div>
        <div className='divBottom'/>
      </div>

      <Footer />
    </div>
  )
}

export default Skills