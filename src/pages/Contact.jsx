import React from 'react'
import { FaLinkedin, FaGithub, FaGitlab, FaEnvelope, FaWhatsappSquare, FaHtml5, FaCss3Alt, FaReact } from 'react-icons/fa';
import Footer from './footer';


const Contact = () => {
  return (
    <div className='SkillsContainer'>
    <div className='contactContainer'>
      <h2>Contact me <span className='spanCircle2'> ●</span></h2>
      <span className='contactContIcon'>
        <FaEnvelope size={24} className='ContactIcon'/>
        <span className='contactImg'>fatimachavezmoron@gmail.com</span>
        </span>
        <a href="https://www.linkedin.com/in/fatima-chavez-2a4805281/" target="_blank" rel="noopener noreferrer">
        <span className='contactContIcon'>
          <FaLinkedin size={24} className='ContactIcon'/>
          <span className='contactImg3'>&nbsp; &nbsp;  Linkedin</span>
          </span>
        </a>
        <a href="https://github.com/fatimachavezmoron" target="_blank" rel="noopener noreferrer">
        <span className='contactContIcon'>
          <FaGithub size={24} className='ContactIcon'/>
          <span className='contactImg3'>&nbsp; &nbsp;  Github</span>
          </span>
        </a>
        <a href="https://gitlab.com/fatimachavezmoron" target="_blank" rel="noopener noreferrer">
          <span className='contactContIcon'>
          <FaGitlab size={24} className='ContactIcon'/> 
          <span className='contactImg3'>&nbsp; &nbsp;  Gitlab</span>
          </span>
        </a>   
      
          <div className='numberCont'>
          <FaWhatsappSquare size={24} className='ContactIcon'/>
          <span className='whatsappNumer'>+49 1578 1501 931</span>
          </div>
          <h2>This website was developed with <span className='spanCircle2'> ●</span></h2>
          <div className='skillsTech'>
         <div className='D'>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaHtml5 size={24} style={{marginRight:'10px'}}/> &nbsp; HTML5</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaCss3Alt size={24} style={{marginRight:'10px'}}/> &nbsp; CSS</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/> &nbsp; React</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/> &nbsp; JSX</p>
         </div>
        </div>
    </div>
    <Footer />
    </div>
  )
}

export default Contact