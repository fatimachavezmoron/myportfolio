import React from 'react'
import { FaLinkedin, FaGithub, FaGitlab } from 'react-icons/fa';

const Footer = () => {
  return (
    <div><div className='footer-container'>
    <p>© 2023 Fati, all rights reserved. </p>
    <div className='social-icons'>
      <a href="https://www.linkedin.com/in/fatima-chavez-2a4805281/" target="_blank" rel="noopener noreferrer">
        <FaLinkedin size={24} />
      </a>
      <a href="https://github.com/fatimachavezmoron" target="_blank" rel="noopener noreferrer">
        <FaGithub size={24} />
      </a>
      <a href="https://gitlab.com/fatimachavezmoron" target="_blank" rel="noopener noreferrer">
        <FaGitlab size={24} />
      </a>
      <p>fatimachavezmoron@gmail.com </p>
    </div>
  </div></div>
  )
}

export default Footer