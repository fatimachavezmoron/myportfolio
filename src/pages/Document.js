import React from 'react'
import { NavLink } from 'react-router-dom'
import Footer from './footer'
import homeImg from '../img/homeImg.png'
import Works from '../components/Works';


const Document = () => {
  return (
      <>
       <header className="App-header">
        <div className="App">
          <div className='titleCont'>
           <h2>FATIMA <br/>CHAVEZ</h2>
           <img src={homeImg} alt='homeimg' className='homeImg'/>
           <div className='starsCont'>
           <span>✦</span>
           <span>✦</span>
           <span>✦</span>
           </div>
           <div className='PtitleCont'>
           <p>+49 1578 1501 931</p>
           <p>fatimachavezmoron@gmail.com</p>
           <p>kleinmachnow, 14532, Germany</p>
           </div>
          </div>
          <div className='careerCont'>
          <p>FRONTEND DEVELOPER</p>
          <span className='horizontalLine'></span>
          </div>
          <div className='worksComponentCont'>
           <Works />
          </div>
        </div>
        <div className='ResumeCont'>
        <NavLink to={`${process.env.PUBLIC_URL}/pdf`} rel="">
        <button className='btnCV'>My Resume <span className='spanCircle'>●</span> </button>
        </NavLink>
        <span className='horizontalLine'></span>
        </div>
       </header>
       <Footer />
       </>
  )
}

export default Document