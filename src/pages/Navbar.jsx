import React, { useState } from 'react';
import { FaBars, FaRegWindowClose } from 'react-icons/fa'
import { NavLink } from 'react-router-dom'

const Navbar = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  const closeMenu = () => {
    setMenuOpen(false);
  };
  return (
    <>
    <div className='navCont'>
    <p>● FC</p>
    <input type='checkbox' id='check' checked={menuOpen} onChange={toggleMenu} />
        <label htmlFor='check' className='contIcon'>
          {menuOpen ? (
            <FaRegWindowClose size={28} id='close-icon' />
          ) : (
            <FaBars size={28} id='menu-icon' />
          )}
        </label>
        <nav className={`navbar ${menuOpen ? 'open' : ''}`} onClick={closeMenu}>

        <div className="navlinkCont" >
          <NavLink className="NavLinks"  to={`${process.env.PUBLIC_URL}/`}>Home</NavLink>
          <NavLink className="NavLinks"  to={`${process.env.PUBLIC_URL}/skills`}>Skills</NavLink>
          <NavLink className="NavLinks"  to={`${process.env.PUBLIC_URL}/contact`}>Contact</NavLink>
          <NavLink className="NavLinks"  to={`${process.env.PUBLIC_URL}/about`}>About Me</NavLink>
        </div>
      </nav>
    </div>
    </>
  )
}

export default Navbar