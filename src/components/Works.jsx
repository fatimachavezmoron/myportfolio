import React from 'react'
import artIa from '../img/artIa.png'
import headphones from '../img/headphones.png'
import encuentro from '../img/encuentro.png'
import reactapp from '../img/react-app.png'
import modernbank from '../img/moderbank.png'
import meggie from '../img/twitterClone.png'
import boostrap from '../img/skills/boostrapIc.png'
import tailwind from '../img/tailwind.svg'
import material from '../img/material.svg'
import typescript from '../img/typescript.svg'
import { FaCss3Alt, FaFontAwesome, FaGitlab, FaJs, FaPaypal, FaReact, FaSass, FaGithub } from 'react-icons/fa';
import {  SiNextdotjs, SiMongodb, SiVercel } from 'react-icons/si';

const Works = () => {
  const FaVite = ({ size }) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={size}
      height={size}
      style={{ color: 'rgb(214, 253, 83)' }}
      role="img"
    >
      <title>Vite</title>
      <path
        d="M8.286 10.578.512-8.657a.306.306 0 0 1 .247-.282L17.377.006a.306.306 0 0 1 .353.385l-1.558 5.403a.306.306 0 0 0 .352.385l2.388-.46a.306.306 0 0 1 .332.438l-6.79 13.55-.123.19a.294.294 0 0 1-.252.14c-.177 0-.35-.152-.305-.369l1.095-5.301a.306.306 0 0 0-.388-.355l-1.433.435a.306.306 0 0 1-.389-.354l.69-3.375a.306.306 0 0 0-.37-.36l-2.32.536a.306.306 0 0 1-.374-.316zm14.976-7.926L17.284 3.74l-.544 1.887 2.077-.4a.8.8 0 0 1 .84.369.8.8 0 0 1 .034.783L12.9 19.93l-.013.025-.015.023-.122.19a.801.801 0 0 1-.672.37.826.826 0 0 1-.634-.302.8.8 0 0 1-.16-.67l1.029-4.981-1.12.34a.81.81 0 0 1-.86-.262.802.802 0 0 1-.165-.67l.63-3.08-2.027.468a.808.808 0 0 1-.768-.233.81.81 0 0 1-.217-.6l.389-6.57-7.44-1.33a.612.612 0 0 0-.64.906L11.58 23.691a.612.612 0 0 0 1.066-.004l11.26-20.135a.612.612 0 0 0-.644-.9z"
        fill="rgb(214, 253, 83)"
      ></path>
    </svg>
  );
  
  return (
    <div>
      <div className='imgCont'>
      <div className='workCont'>
        <a href='https://twitter-ten-psi.vercel.app/'>
         <img src={meggie} className='pageImg' alt='pageImg' target="_blank" rel="noopener noreferrer"/>
        </a>
        <div className='techCont'>
         <p >Twitter Clone<span className='spanCircle'>●</span></p> <br/>
         <div className='techIcons'>
         <p style={{fontSize:'24px', color:'white'}}>Technologies:</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/>  React</p>
         <img src={typescript} alt='boostrapImg' className='boostrapIcon' />
         <span>  &nbsp; Typescript</span>
         <p style={{ display: 'flex', alignItems: 'center',}}><SiNextdotjs size={24} style={{marginRight:'10px'}}/>  Next.js</p>
         <img src={tailwind} alt='boostrapImg' className='boostrapIcon' style={{marginRight:'8px'}}/>
         <span>  Tailwind</span>
         <p style={{ display: 'flex', alignItems: 'center',}}><SiMongodb size={24} style={{marginRight:'10px'}}/>  Mongodb</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaGithub size={24} style={{marginRight:'10px'}}/>  Github</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><SiVercel size={24} style={{marginRight:'10px'}}/>  Vercel</p>
         </div>
        </div>
        </div>
      <div className='workCont'>
        <a href='https://fatimachavezmoron.gitlab.io/art-ai/' target="_blank" rel="noopener noreferrer">
            <img src={artIa} className='pageImg' alt='pageImg' />
        </a>
        <div className='techCont'>
        <p>Art AI<span className='spanCircle'>●</span></p> <br/>
         <div className='techIcons'>
         <p style={{fontSize:'24px', color:'white'}}>Technologies:</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/>  React</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaJs size={24} style={{marginRight:'10px'}}/>  Javascript</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaCss3Alt size={24} style={{marginRight:'10px'}}/>  CSS</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaGitlab size={24} style={{marginRight:'10px'}}/>  Gitlab</p>
         </div>
        </div>
        </div>
        <div className='workCont'>
        <a href='https://fatimachavezmoron.gitlab.io/eshop/' target="_blank" rel="noopener noreferrer">
         <img src={headphones} className='pageImg' alt='pageImg' />        
        </a>
        <div className='techCont'>
        <p>Headphones<br/> Eshop<span className='spanCircle'>●</span></p> <br/>
         <div className='techIcons'>
         <p style={{fontSize:'24px', color:'white'}}>Technologies:</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/>  React</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaJs size={24} style={{marginRight:'10px'}}/>  Javascript</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaSass size={24} style={{marginRight:'10px'}}/>  SASS</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaFontAwesome size={24} style={{marginRight:'10px'}}/>  Font Awesome </p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaPaypal size={24} style={{marginRight:'10px'}}/>  Paypal </p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaGitlab size={24} style={{marginRight:'10px'}}/>  Gitlab</p>
         </div>
        </div>
        </div>
         <div className='workCont'>
        <a href='https://encuentroestenopeico.com/'>
         <img src={encuentro} className='pageImg' alt='pageImg' target="_blank" rel="noopener noreferrer"/>
        </a>
        <div className='techCont'>
        <p>Encuentro <br/>Estenopeico<span className='spanCircle'>●</span></p> <br/>
         <div className='techIcons'>
         <p style={{fontSize:'24px', color:'white'}}>Technologies:</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaReact size={24} style={{marginRight:'10px'}}/>  React</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaJs size={24} style={{marginRight:'10px'}}/>  Javascript</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaCss3Alt size={24} style={{marginRight:'10px'}}/>  CSS</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaGitlab size={24} style={{marginRight:'10px'}}/>  Gitlab</p>
         </div>
        </div>
        </div>
         <div className='workCont'>
        <a href='https://fatimachavezmoron.github.io/apps-page/'>
         <img src={reactapp} className='pageImg' alt='pageImg' target="_blank" rel="noopener noreferrer"/>
        </a>
        <div className='techCont'>
        <p>React's Apps<span className='spanCircle'>●</span></p> <br/>
         <div className='techIcons'>
         <p style={{fontSize:'24px', color:'white'}}>Technologies:</p>
         <span className='boostrapIcon2'>
          <img src={boostrap} alt='boostrapImg' style={{marginTop:'10px' ,marginRight:'10px', width:'26px', }}/>
          <span style={{marginTop:'14px' }}>Boostrap</span> 
          </span>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaVite size={24} /> &nbsp; Vite</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaJs size={24} style={{marginRight:'10px'}}/>  Javascript</p>
         <img src={material} alt='boostrapImg' className='boostrapIcon' style={{marginRight:'10px'}}/>
         <span>  Material UI</span>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaCss3Alt size={24} style={{marginRight:'10px',}}/>  CSS</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaGithub size={24} style={{marginRight:'10px'}}/>  Github</p>
         </div>
        </div>
        </div>
        <div className='workCont'>
        <a href='https://bank-landingpage-withvite.netlify.app/'>
         <img src={modernbank} className='pageImg' alt='pageImg' target="_blank" rel="noopener noreferrer"/>
        </a>
        <div className='techCont'>
        <p>Modern Bank<span className='spanCircle'>●</span></p> <br/>
         <div className='techIcons'>
         <p style={{fontSize:'24px', color:'white'}}>Technologies:</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaVite size={24} /> &nbsp; Vite</p>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaJs size={24} style={{marginRight:'10px'}}/>  Javascript</p>
         <img src={tailwind} alt='boostrapImg' className='boostrapIcon' style={{marginRight:'8px'}}/>
         <span>  Tailwind</span>
         <p style={{ display: 'flex', alignItems: 'center',}}><FaGitlab size={24} style={{marginRight:'10px'}}/>  Gitlab</p>
         </div>
        </div>
        </div>
      </div>
    </div>
  )
}

export default Works